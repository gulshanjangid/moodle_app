package com.example.gulshan.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    public static String server="http://tapi.cse.iitd.ernet.in:1805/";
    public static String url = "";
    public static String username = "";
    public static String password = "";
    public static String first_name = "";
    public static String last_name = "";
    public static String entry_no = "";
    public static String email = "";
    public static Boolean success = false;
    public static int type;
    public static int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Login(View view){
        EditText username_edit = (EditText)findViewById(R.id.username);
        EditText password_edit = (EditText)findViewById(R.id.password);
        username = "userid=" + username_edit.getText().toString() + "&";
        password = "password=" + password_edit.getText().toString();
        url = server + "default/login.json?" + username + password;

        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // the response is already constructed as a JSONObject!
                        try {
                            success = response.getBoolean("success");
                            if (success){
                                response = response.getJSONObject("user");
                                first_name = response.getString("first_name");
                                last_name = response.getString("last_name");
                                entry_no = response.getString("entry_no");
                                email = response.getString("email");
<<<<<<< HEAD
                                type = response.getInt("type_");
                                id = response.getInt("id");
                                Toast.makeText(MainActivity.this, first_name + last_name, Toast.LENGTH_LONG).show();
                                Intent i = new Intent(MainActivity.this, AfterLogin.class);
                                startActivity(i);
=======
                                type = response.getInt("type");
                                id = response.getInt("id");
                                Toast.makeText(MainActivity.this, first_name + last_name, Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(MainActivity.this, AfterLogin.class);
                                    startActivity(i);
>>>>>>> 5ff0d1f933dbb303c88fceb0b1c64b13aa7e153b
                            }else{
                                Toast.makeText(MainActivity.this, "Invalid UserName or Password", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(this).add(jsonRequest);
    }





}