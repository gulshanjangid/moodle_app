package com.example.gulshan.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AssignmentsView extends AppCompatActivity {

    //set assignmentsList such that you can get assignment number from it

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignments_view);
        populateListView();
        registerCallback();
    }

    public void populateListView(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.assignments_list,AfterCourseTouch.AssignmentsListOfThisCourse);

        ListView AssignmentsList = (ListView) findViewById(R.id.AssignmentsView);
        AssignmentsList.setAdapter(adapter);
    }

    public void registerCallback(){
        ListView AssignmentsList = (ListView) findViewById(R.id.AssignmentsView);
        AssignmentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                String message = textView.getText().toString();



                /*
                DO something with the assignment using its assignment number

                 */

            }
        });

    }

}
