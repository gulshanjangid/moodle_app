package com.example.gulshan.moodle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class gradesView extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades_view);

        populateListView();
        registerCallback();
    }

public void populateListView(){
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.gradeslist,AfterLogin.AllgradesView);

    ListView gradesList = (ListView) findViewById(R.id.gradesView);
    gradesList.setAdapter(adapter);
}

public void registerCallback(){
    ListView gradesList = (ListView) findViewById(R.id.gradesView);
    gradesList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = (TextView) view;

            String message = "You clicked number" + position + ",which is string:" + textView.getText().toString();
            Toast.makeText(gradesView.this, message, Toast.LENGTH_LONG).show();
        }
    });

}


}
