package com.example.gulshan.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class coursesView extends AppCompatActivity {
    public static String assignments = "";
    public static  String grades = "";
    public static String threads = "";
    public static String touchedCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses_view);

        populateListView();
        registerCallback();
    }

    public void populateListView(){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.courses_list,AfterLogin.courses);

        ListView coursesList = (ListView) findViewById(R.id.coursesView);
        coursesList.setAdapter(adapter);
    }

    public void registerCallback(){
        ListView coursesList = (ListView) findViewById(R.id.coursesView);
        coursesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                String message = "You clicked number" + position + ",which is string:" + textView.getText().toString();

                touchedCourse = textView.getText().toString();
                assignments = "See assignments of " + textView.getText().toString();
                grades = "See grades of " + textView.getText().toString();
                threads = "See threads related to " + textView.getText().toString();


                launchActivity();

            }
        });

    }

    public void launchActivity(){
        Intent i = new Intent(this,AfterCourseTouch.class);
        startActivity(i);
    }

}
