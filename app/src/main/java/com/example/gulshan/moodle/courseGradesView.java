package com.example.gulshan.moodle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class courseGradesView extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_grades_view);
        populateListView();

    }

    public void populateListView(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.course_grades_view_list,AfterCourseTouch.GradesListOfThisCourse);

        ListView GradesListOfThisCourse = (ListView) findViewById(R.id.courseGradesView);
        GradesListOfThisCourse.setAdapter(adapter);
    };

}
