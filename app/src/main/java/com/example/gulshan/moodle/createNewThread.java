package com.example.gulshan.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class createNewThread extends AppCompatActivity {
    public static String urlMinus = "http://tapi.cse.iitd.ernet.in:1805/threads/new.json?title=";
    public static String descriptionMinus = "&description=";
    public static String title;
    public static String description;
    public static String courseCode;
    public  static  String courseCodeMinus = "&course_code=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_thread);
    }

    public void CreateThread(View view) {
        EditText title_edit = (EditText) findViewById(R.id.title);
        String title = title_edit.getText().toString();
        EditText description_edit = (EditText) findViewById(R.id.description);
        String description = description_edit.getText().toString();
        Toast.makeText(createNewThread.this, "success", Toast.LENGTH_LONG).show();

        String url = urlMinus + title + descriptionMinus + description + courseCodeMinus + coursesView.touchedCourse.substring(0, 6);


        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(createNewThread.this, response.toString(), Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(this).add(jsonRequest);









    }
}
