package com.example.gulshan.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AfterCourseTouch extends AppCompatActivity {
    public static String urlMinus = "http://tapi.cse.iitd.ernet.in:1805/courses/course.json/";
    public static String urlMiddle;
    public static String urlPlusA = "/assignments";
    public static String urlPlusG = "/grades";
    public static  String urlPlusT = "/threads";
    public static String urlA;
    public static String urlG;
    public static String urlT;
    public static String[] AssignmentsListOfThisCourse;
    public static String[] GradesListOfThisCourse;
    public static String[] ThreadsOfThisCourse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_course_touch);

        coursesView a = new coursesView();

        TextView assignments_text = (TextView) findViewById(R.id.assignments_text);
        assignments_text.setText(a.assignments);


        TextView grades_text = (TextView) findViewById(R.id.grades_text);
        grades_text.setText(a.grades);

        TextView threads_text = (TextView) findViewById(R.id.threads_text);
        threads_text.setText(a.threads);
    }

public void assignmentsClick(View view){


    urlMiddle = coursesView.touchedCourse.substring(0, 6);
    urlA  = urlMinus + urlMiddle + urlPlusA;



    JsonObjectRequest jsonRequest = new JsonObjectRequest
            (Request.Method.GET, urlA, (String) null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // the response is already constructed as a JSONObject!
                    try {
                        JSONArray assignments = response.getJSONArray("assignments");


                        if(assignments.length()==0){
                            AssignmentsListOfThisCourse = new String[1];
                            AssignmentsListOfThisCourse[0]= "No assignments posted";
                        }
                        else {
                            AssignmentsListOfThisCourse = new String[assignments.length()];
                            for(int i =0;i<assignments.length();i++){
                                JSONObject object = assignments.getJSONObject(i);
                                String createdAt = object.getString("created_at");
                                String lateDaysAllowed = object.getString("late_days_allowed");
                                String name = object.getString("name");
                                String deadline = object.getString("deadline");
                                String description = object.getString("description");

                                String visible = name  + " , created at: " + createdAt + "    deadline: " + deadline + ",    late days allowed: "+ lateDaysAllowed;

                                AssignmentsListOfThisCourse[i] = visible;

                            }
                        }



                        Intent i = new Intent(AfterCourseTouch.this,AssignmentsView.class);
                        startActivity(i);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

    Volley.newRequestQueue(this).add(jsonRequest);








}


public void threadsClick(View view) {

    urlMiddle = coursesView.touchedCourse.substring(0, 6);
    urlT  = urlMinus + urlMiddle + urlPlusT;



    JsonObjectRequest jsonRequest = new JsonObjectRequest
            (Request.Method.GET, urlT, (String) null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // the response is already constructed as a JSONObject!
                    try {
                        JSONArray threads = response.getJSONArray("course_threads");


                        if(threads.length()==0){
                            ThreadsOfThisCourse = new String[1];
                            ThreadsOfThisCourse[0]= "No threads displayed";
                        }
                        else {
                            ThreadsOfThisCourse = new String[threads.length()];
                            for(int i =0;i<threads.length();i++){
                                JSONObject object = threads.getJSONObject(i);
                                String Title = object.getString("title");
                                String description = object.getString("description");
                                String createdAt = object.getString("created_at");


                                String visible = Title  + " ," +
                                        "      description: " + description + "  ,created at: " + createdAt;

                                ThreadsOfThisCourse[i] = visible;

                            }
                        }


                        Intent j = new Intent(AfterCourseTouch.this,courseThreadsView.class);
                        startActivity(j);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

    Volley.newRequestQueue(this).add(jsonRequest);


    }

public void gradesClick(View view) {

    urlMiddle = coursesView.touchedCourse.substring(0, 6);
    urlG  = urlMinus + urlMiddle + urlPlusG;



    JsonObjectRequest jsonRequest = new JsonObjectRequest
            (Request.Method.GET, urlG, (String) null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // the response is already constructed as a JSONObject!
                    try {
                        JSONArray grades = response.getJSONArray("grades");


                        if(grades.length()==0){
                            GradesListOfThisCourse = new String[1];
                            GradesListOfThisCourse[0]= "No grades displayed";
                        }
                        else {
                            GradesListOfThisCourse = new String[grades.length()];
                            for(int i =0;i<grades.length();i++){
                                JSONObject object = grades.getJSONObject(i);
                                String weightage = object.getString("weightage");
                                String score = object.getString("score");
                                String name = object.getString("name");
                                String outOf = object.getString("out_of");


                                String visible = name  + " , Score: " + score + " out of: " + outOf + ",    weightage: "+ weightage;

                                GradesListOfThisCourse[i] = visible;

                            }
                        }

                        Intent k = new Intent(AfterCourseTouch.this,courseGradesView.class);
                        startActivity(k);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

    Volley.newRequestQueue(this).add(jsonRequest);






    }

    public void createNewThread(View view) {
        Intent l = new Intent(AfterCourseTouch.this,createNewThread.class);
        startActivity(l);



    }
}
